import math
import random
import subprocess
import copy 
import time

import solid
import parts
import parts.base
import parts.basic
import parts.cube_holder
from parts import tree

SEGMENTS = 100

def generate_city(seed, max_height = 100, min_width_center = 4, min_feature = 0.4):
  city = parts.base.hex(seed, max_height)  
  open_spots = [(spot, 0) for spot in city.spots]
  highest_part = 0
  while open_spots:
    (spot, height) = open_spots.pop(0)

    if height < max_height:
      highest_part = max(highest_part, height)  
      (new_part, new_spots) = parts.basic.random_part(spot, min_feature)
      if new_part:
        spot.attach(new_part, new_spots)      
      else:
        #print(f"Couldn't find a part for spot {spot.size} {spot.Shape}")
        pass
      
      #If this is the central tower spot, check that the next spot will be big enough
      if spot.isCentralTower:
        next_central_tower_spot = next(filter(lambda spot: spot.isCentralTower == True, spot.spots), None)

        #If the next central tower spot wouldn't be useable, build the player holder now 
        if (
          next_central_tower_spot is None 
          or next_central_tower_spot.effective_size(tree.Shape.square) < min_width_center 
          or (next_central_tower_spot.location.height() + height) > max_height
          ):
          #print(f"Building cube holder. Next tower spot effective size was {next_central_tower_spot.effective_size(tree.Shape.square)}")
          cube_holder = parts.cube_holder.player(spot.effective_size(tree.Shape.square))          
          spot.attach(cube_holder)

      open_spots.extend(map(lambda new_spot: (new_spot, height+new_spot.location.height()), spot.spots))
    #solid.scad_render_to_file(city.render_scad(), file_header=f'$fn = {SEGMENTS};', include_orig_code=False)

  return city, highest_part

def move_to_hex_tile_position(part, row, column, inradius = 17.5, side_length = 20.2, gap = 2.5):
  forward_spacing = ((side_length * 1.5) + gap) * row
  right_spacing = ((inradius * 2) + gap) * column
  if row % 2 != 0:
    right_spacing += inradius + gap/2  
      
  return solid.translate([right_spacing, forward_spacing])(part)  

def generate_city_height_range(min = 10, max = 40):
  while True:
    seed = random.randrange(1,10000)
    random.seed(seed)
    (city, city_height) = generate_city(seed, max_height = max)        
    if min <= city_height <= max:
      print(f"Returning city of height {city_height}")
      return city, seed, city.render_scad()
    else:
      #print(f"Discarded city of height {city_height}")
      pass  

def tile_cities(height, width, inradius = 17.5, side_length = 20.2, gap = 2.5, min_height = 0, max_height = 100, offset_rows = 0):
  cities = solid.union()  
  city_renders = []
  
  for y in range(0, height):
    for x in range(0, width):
      (city,seed,city_scad) = generate_city_height_range(min_height, max_height)        
      city_path = f"cities\Seed_{seed}"      
      solid.scad_render_to_file(city_scad, file_header=f'$fn = {SEGMENTS};', include_orig_code=False, filepath=city_path + ".scad")      
      cities += move_to_hex_tile_position(city_scad, x+offset_rows, y, inradius, side_length, gap)      

  for i in range(len(city_renders)):        
    print(f"Rendering {i}")
    city_renders[i].wait()
    print(f"\tFinished {i}")
  return cities

def city_from_seed(seed):
  random.seed(seed)  
  (city, city_height) = generate_city(seed)
  city_scad = city.render_scad()
  return city_scad

def increasing_city_sizes():
  city_scad = solid.union()
  for i in range(0,8):
    city_scad += tile_cities(5,1, min_height = i*10, max_height = (i+1)*10, offset_rows=i)  
    print(f"finished row {i}")
    solid.scad_render_to_file(city_scad, file_header=f'$fn = {SEGMENTS};', include_orig_code=False)
  return city_scad  

if __name__ == '__main__':      
  #TODO: Take a seed as an argument
  seed = random.randrange(1,10000)
  
  city_scad = city_from_seed(8286)

  '''
  total_time = 0
  trials = 100
  for i in range(0, trials):
    seed = random.randrange(1,10000)
    start_time = time.monotonic()  
    city_scad = city_from_seed(seed)
    end_time = time.monotonic()
    total_time += end_time - start_time
  average_time = total_time / trials
  print(f"On average generation took {average_time} seconds")
  '''

  #(city, seed, city_scad) = generate_city_height_range()  

  #city_scad = tile_cities(5,5, min_height=10) #max_height=40)  
  
  #city_scad = increasing_city_sizes()

  solid.scad_render_to_file(city_scad, file_header=f'$fn = {SEGMENTS};', include_orig_code=False)  
