import math
import solid
from solid import utils
import numpy as np

import parts
from parts import tree

def render(size):  
  (solid, spots) = raised_platform(size)  
  return [(solid, spots, 1)]

def raised_platform(width, height_ratio = 0.3, width_ratio = 0.5):  
  platform_height = width * height_ratio
  under_build_ratio = 0.8 #How much of the available space under the overhang should be used 
  platform_width = platform_height * 0.05 
  
  platform = utils.up(platform_height/2)( solid.cube((width, width, platform_height), center = True) )    
  raised_platform = tree.Part("raised_platform", tree.Shape.square, width, platform_width, platform)  

  #Set up a cylinder off to the side so removing it will leave sweeping archs on the platform  
  cut_height = platform_height - platform_width
  cut_width = (width / 2) * width_ratio
  cut = solid.rotate((90,0,0))(solid.cylinder(r = 1, h= 1, center=True))
  cut = solid.scale([cut_width, width*2, cut_height])(cut)
  cut = utils.right(width/2)(cut) 

  #Swing our cut cylinder around to each side
  for rotation in np.linspace(0, 360, num = 4, endpoint = False):
    raised_platform.solid -= solid.rotate([0,0,rotation])(cut)

  spots = []
  spots.append(tree.Spot(tree.Shape.square, width, translation=(0,0,platform_height), isCentralTower=True))

  #Place cities on the edge under the overhang 
  side_city_width = cut_width * under_build_ratio
  side_city_distance = (width/2) - (side_city_width/2) 
  side_city_maximum_height = platform_height * 0.7
  spots.extend(tree.Spot(tree.Shape.square, side_city_width, maximum_height=side_city_maximum_height, translation=(side_city_distance, 0, 0)).rotated(4))
  
  #Place cities on the corners under the overhang  
  corner_city_radius = math.hypot(side_city_distance - width/2, side_city_distance - width/2) * under_build_ratio
  corner_city_maximum_height = platform_height * 0.7
  spots.extend(tree.Spot(tree.Shape.circle, corner_city_radius, maximum_height=corner_city_maximum_height, translation=(side_city_distance, side_city_distance, 0)).rotated(4))  
  
  return raised_platform, spots