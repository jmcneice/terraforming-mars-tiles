import math
import solid
from solid import utils
import numpy as np

import parts
from parts import tree

def render(size):  
  (solid, spots) = corner_support_platform(size, 4)  
  return [(solid, spots, 1)]

#TODO: This was designed to have the same size on the top and bottom but it's currently shrinking by a bit. 
def corner_support_platform(width, column_sides):          
  column_top = width * 0.2
  column_bottom = width * 0.1
  column_height = width * 0.3
  column_position = (width/2 - column_top, width/2 - column_top, 0)

  distance_to_column = math.hypot(column_position[0], column_position[0])
  distance_to_inner_corner = distance_to_column - column_bottom
  distance_to_outer_corner = distance_to_column + column_top

  top_size = distance_to_outer_corner
  top_width = width * 0.1  
  
  platform = tree.Part("corner_supported_platform", tree.Shape.square, width, top_width, solid.union())

  #Put a tapered column in each table
  tapered_column = solid.cylinder(r1 = column_bottom, r2 = column_top, h = column_height, segments=column_sides)
  tapered_column = solid.rotate([0,0,45])(tapered_column)
  tapered_column = solid.translate(column_position)(tapered_column)
  for rotation in np.linspace(0, 360, num = 4, endpoint = False):
    platform.solid += solid.rotate([0,0,rotation])(tapered_column)

  #Put a large cube in the center to fill space. Have it just touch the upper corners
  #We use a 4-sided cylinder to make it easier to size with the beveled top    
  center_size = distance_to_inner_corner * 1.0
  center_block = solid.rotate([0,0,45])(solid.cylinder(r1 = center_size, r2 = center_size, h = column_height, segments=4))
  platform.solid += center_block
  
  #Put a beveled top on
  top_block = solid.rotate([0,0,45])(solid.cylinder(r1 = center_size, r2 = top_size, h = top_width, segments=4))
  top_block = solid.translate([0,0, column_height - top_width])(top_block)  
  platform.solid += top_block

  spots = []
  top_area = math.hypot(top_size, top_size)
  spots.append(
    tree.Spot(
      tree.Shape.square,
      top_area,
      (0,0,column_height),
      isCentralTower=True,
    )
  )

  #Place cities on the edge under the overhang 
  side_city_inner_limit = math.sqrt((center_size**2) / 2)
  side_city_outer_limit = width/2
  side_city_width = (side_city_outer_limit - side_city_inner_limit)/2
  spots.extend(
    tree.Spot(
      tree.Shape.square,
      side_city_width,
      (side_city_inner_limit + side_city_width/2, 0, 0),
      maximum_height=column_height - top_width,
    )
    .rotated(4)
  )

  return platform, spots