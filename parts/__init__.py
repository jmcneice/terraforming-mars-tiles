import parts.raised_platform
import parts.tunnels
import parts.tower
import parts.gear
import parts.fin_column
import parts.dome
import parts.corner_supported_platform
import parts.split_in_four

#TODO: There has to be some way to inspect and generate this list automatically. 
modules = (
  parts.raised_platform,
  parts.tower,
  parts.tunnels,
  parts.gear,
  parts.fin_column,
  parts.dome,
  parts.corner_supported_platform,
  parts.split_in_four,
  )

'''
import imp,os
def __load_all__(dir="definitions"):
  list_modules=os.listdir(dir)
  list_modules.remove('__init__.py')
  list_modules.remove('part.py')
  modules = []
  for module_name in list_modules:
      if module_name.split('.')[-1]=='py':
          print(f"Load module {module_name}")
          foo = imp.load_source('module', dir+os.sep+module_name)
          part = foo.render(10)
          print(f"Loaded {part.name}")
          modules.append(foo)
          return modules
  return modules
'''