import unittest
import solid
import math
from parts import tree

class TestSpotAttachments(unittest.TestCase):
  #When a part with the same shape is attached it should be resized to the spots size. 
  def test_resize_same_shape(self):
    base_spot = tree.Spot(tree.Shape.circle, 10)  
    part = tree.Part("", tree.Shape.circle, 5, 1, solid.union())
    attached_spot = tree.Spot(tree.Shape.circle, 2)
    base_spot.attach(part, [])
    self.assertEqual(part.size, 10)

  #When a square part is being attached to a circular spot it should be resized to the
  # largest square that will fit in the circle. 
  def test_inscribe_square_in_circle(self):
    circle_radius = 10
    circle_diameter = circle_radius*2
    inscribed_square_diagonal = circle_diameter
    inscribed_square_width = inscribed_square_diagonal / math.sqrt(2)
    
    base_spot= tree.Spot(tree.Shape.circle, circle_radius)
    part = tree.Part("", tree.Shape.square, 50, 1, solid.union())
    base_spot.attach(part, [])
    self.assertAlmostEqual(part.size, inscribed_square_width, places=7)

  #When a circular part is being attached to a square spot it should be resized to the
  # largest circle that will fit in the square
  def test_inscribe_circle_in_square(self):
    square_width = 10
    inscribed_circle_radius = square_width/2

    base_spot= tree.Spot(tree.Shape.square, square_width)
    part = tree.Part("", tree.Shape.circle, 50, 1, solid.union())
    base_spot.attach(part, [])
    self.assertEqual(part.size, inscribed_circle_radius)    

if __name__ == '__main__':
  unittest.main()