import copy
import enum
import solid
import numpy as np
import subprocess
import os
import filecmp
import math

#We can't use the build in solid.scad_render_to_file because it puts a timestamp at the top preventing us from comparing the files later
import solid.solidpython as solidpython_internals
def scad_render_to_file(object, path):
  assert(object is not None), "Can't render a None object."
  scad = solid.scad_render(object, "$fn = 100;")
  return solidpython_internals._write_code_to_file(scad, path, None, False)  

class Shape(enum.Enum):
  circle = 1
  square = 2  

class Location:
  def __init__(self, translation=None, rotation=None):
    self.translation = translation
    self.rotation = rotation    

    if self.translation is None:
      self.translation = (0,0,0)
    
    if self.rotation is None:
      self.rotation = (0,0,0)

  #Returns a list of locations with the same translation and evenly spaced rotation around Z-axis
  def rotated(self, num):
    rotated_locations = []
    for rotation in np.linspace(0, 360, num = num, endpoint = False):      
      rotated_location = copy.copy(self)
      rotated_location.rotation = (0,0,rotation)
      rotated_locations.append(rotated_location)
    return rotated_locations

  def transformation(self):        
    rotation = solid.rotate(self.rotation)
    translation = solid.translate(self.translation)
    union = solid.union()

    if self.rotation != (0,0,0) and self.translation != (0,0,0):    
      rotation.add(translation)
      translation.add(union)
      return rotation, union
    elif self.rotation != (0,0,0):
      rotation.add(union)
      return rotation, union
    elif self.translation != (0,0,0):
      translation.add(union)
      return translation, union
    else:      
      return union, union

  def x(self):
    return self.translation[0]
  
  def y(self):
    return self.translation[1]    

  def height(self):
    return self.translation[2]

  def scale(self, factor):
    self.translation = list((axis*factor for axis in self.translation))

class Spot:
  def __init__(self, shape, size, translation=None, rotation=None, location=None, isCentralTower=False, maximum_height=math.inf):    
    #A spot is an area with a particular shape, size, and location. 
    assert(shape is not None)
    self.shape = shape    

    assert(size is not None)
    self.size = size        
    
    assert((translation is None and rotation is None) or (location is None)), "If the location argument is provided for a space then you must not provide a translation/rotation."
    if location:
      self.location = location
    else:    
      self.location = Location(translation, rotation)

    #A spot can marked as being the central tower. This will be passed down to any following spots.
    self.isCentralTower = isCentralTower

    #A spot may have a part placed in it or may be left empty.
    self.part = None

    #A spot may have subspots
    self.spots = []

    self.maximum_height = maximum_height

  def effective_size(self, shape):
    if self.shape == shape:
      return self.size
    elif self.shape == Shape.circle and shape == Shape.square:
      return self.size * math.sqrt(2) #inscribed square width
    elif self.shape == Shape.square and shape == Shape.circle:
      return self.size / 2 #incscribed circle width
    else:
      raise NotImplementedError(
        f"A {part.shape} part being sized for a {self.shape} isn't implemented"
      )      

  def overlaps(self, spot):
    if self.shape == spot.shape == Shape.circle:
      distance_between_centers = math.hypot(self.location.x() - spot.location.x(), self.location.y() - spot.location.y())
      return distance_between_centers <= self.size + spot.size
    if self.shape == Shape.square and spot.shape == Shape.circle:
      #TOOD: Implement this correctly. Right not it's just scribing a circle around the square and calling it a day
      distance_between_centers = math.hypot(self.location.x() - spot.location.x(), self.location.y() - spot.location.y())
      return distance_between_centers <= self.size*math.sqrt(2)/2 + spot.size
    else:
      raise NotImplementedError(f"Comparing parts of shapre {self.shape} and {spot.shape} isn't implemented")  
      
  def scale(self, factor):
    self.size *= factor
    self.location.scale(factor)
    self.maximum_height *= factor
    
    assert(self.part is None), "Scaling a spot after a part is attached isn't implemented."
    assert(not self.spots), "Scaling a spot after spots are attached isn't implemented."

  #Returns a list of spots with the same size/translation and evenly spaced rotation around Z-axis
  def rotated(self, num):
    assert(self.part is None), "Rotating a spot after a part is attached isn't implemented."
    assert(not self.spots), "Rotating a spot after spots are attached isn't implemented."
    return map(
      lambda location: Spot(
        self.shape, 
        self.size, 
        location=location, 
        isCentralTower=self.isCentralTower,
        maximum_height=self.maximum_height,
      ), 
      self.location.rotated(num)
    )      

  #Attach a part and related spots to this spot. Replaces any previous part and related spots.
  def attach(self, part, spots = None):
    if spots is None:
      spots = []

    scale_factor = 1
    if part:
      scaled_size = self.effective_size(part.shape)    
      scale_factor = scaled_size / part.size      
      part.scale(scale_factor)          

    for spot in spots:  
      if scale_factor != 1:
        spot.scale(scale_factor)

      if not self.isCentralTower:      
        spot.isCentralTower = False

      #If attaching the spot would create a height greater than 
      #the maximum height for this spot, don't attach anything
      if spot.location.height() > self.maximum_height:
        return    
              
      spot.maximum_height = min(self.maximum_height - spot.location.height(), spot.maximum_height)

    self.part = part
    self.spots = spots
  
  def render_scad(self):
    (root, leaf) = self.location.transformation()    

    if self.part:
      leaf.add(self.part.solid)

    for spot in self.spots:
      spot_scad = spot.render_scad()
      if spot_scad:
        leaf.add(spot_scad)

    if leaf.children:
      return root      
    else:
      return None

class Part:
  def __init__(self, name, shape, size, smallest_feature, solid, render=True):
    self.name = name
    self.shape = shape
    
    #The size is the characteristic size of the part based on its shape (width for square, radius for circles)
    self.size = size    
    #The smallest feature is the width the smallest feature in this part. It's used to decide if the part can be accurately printed.
    self.smallest_feature = smallest_feature    

    self.solid = solid      

    #If render is false then this part can not be pre-rendered to an stl file
    self.render = render
    
  def scale(self, factor):
    self.size *= factor
    self.smallest_feature *= factor
    self.solid = solid.scale((factor, factor, factor))(self.solid)   

  def render_stl(self):
    if not self.render:
      return

    assert self.name is not None, "A parts name is used to name the stl file, so unnamed parts can't be rendered."
    renders_folder = f"parts/rendered/"

    scad_path = f"{renders_folder}/{self.name}_{self.size}.scad"    
    stl_path = f"{renders_folder}/{self.name}_{self.size}.stl"
    
    scad_file_exists = os.path.exists(scad_path)
    stl_file_exists = os.path.exists(stl_path)
    
    new_scad_path = f"{renders_folder}/{self.name}_{self.size}_new.scad"    
    scad_render_to_file(self.solid, new_scad_path)
    #If the new scad file is the same as the old scad file, and a render of the old file exists, then no need to rerender
    if scad_file_exists and filecmp.cmp(scad_path, new_scad_path) and stl_file_exists:
      os.remove(new_scad_path)      
    #Something is new or changed so we'll render the scad to stl
    else:
      print(f"{self.name} changed, rerendering")
      if scad_file_exists:
        os.remove(scad_path)            
      os.rename(new_scad_path, scad_path)       

      #Use openscad to render the scad file to an stl    
      render = subprocess.Popen(["C:\Program Files\OpenSCAD\openscad.exe", "-o", stl_path, scad_path])
      render.wait()      
      print(f"\t{self.name} done")
      
    #Replace our solid object with a command to import the rendered stl
    self.solid = solid.import_(stl_path)      




