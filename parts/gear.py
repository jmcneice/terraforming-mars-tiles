import math
import solid
from solid import utils
import numpy as np
from parts import tree

def render(size):  
  possible_teeth = range(6,9)
  weight = 1/len(possible_teeth)

  parts = []
  for teeth in possible_teeth: 
    (solid, spots) = gear(size, teeth)
    parts.append((solid, spots, weight))

  return parts

def gear(radius, num_teeth):
  gear_base_height = radius * 0.3
  gear_top_radius = radius * 0.8
  gear_top_height = radius * 0.1
  gear_height = gear_base_height + gear_top_height
 
  gear_base = solid.cylinder(r=radius,h=gear_base_height)
  beveled_top = solid.utils.up(gear_base_height)(solid.cylinder(r1=radius,r2=gear_top_radius,h=gear_top_height) )
  gear = tree.Part(f"gear_{num_teeth}", tree.Shape.circle, radius, gear_top_height, gear_base + beveled_top)  

  cutter = solid.cylinder(r=radius*0.2,h=gear_height*2)
  cutter = solid.utils.right(radius)(cutter)
  for rotation in np.linspace(0, 360, num = num_teeth, endpoint = False):
    gear.solid -= solid.rotate([0,0,rotation])(cutter)

  spots = [
    tree.Spot(
      tree.Shape.circle, 
      gear_top_radius, 
      translation=(0,0,gear_height),
      isCentralTower=True
    )
  ]

  return (gear, spots)         