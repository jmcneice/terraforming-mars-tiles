import math
import solid
import numpy as np
from parts import tree

def render(size):  
  possible_fins = range(2,4)
  weight = 1/len(possible_fins)

  parts = []
  for fins in possible_fins: 
    (solid, spots) = fin_column(size, fins)
    parts.append((solid, spots, weight))

  return parts

def fin_column(radius, num_fins):
  fin_height = radius * 0.2     
  fin_bottom = radius * 0.7
  fin_top = radius * 0.9
  fin = solid.cylinder(r1=fin_bottom, r2=fin_top, h=fin_height)

  #Stack up the small fins
  fin_stack = tree.Part(f"fin_column_{num_fins}", tree.Shape.circle, radius, fin_height, solid.union())  
  for level in range(0,num_fins):
    fin_stack.solid += solid.utils.up(fin_height * level)(fin)

  #Put a larger fin on top as the base for the next part
  #large_fin_top = radius * 1.0
  #fin_stack.solid += solid.utils.up(fin_height * (num_fins-1))(solid.cylinder(r1=fin_top, r2=large_fin_top, h=fin_height))
  
  spots = [
    tree.Spot(
      tree.Shape.circle,
      fin_top,
      (0,0,fin_height*num_fins),
      isCentralTower=True
    )
  ]  
  
  return fin_stack, spots