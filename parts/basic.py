from parts import modules
from parts import tree
import random
import copy

class PartInventory:
  def __init__(self):
    self.parts = []    
    self._sorted = False

  def add(self, part, spots, weight):
    self.parts.append((part, spots, weight))
    self._sorted = False

  def weight(self, start = 0):
    return sum(weight for (part, spots, weight) in self.parts[start:])    

  def sort(self):
    self.parts.sort(key=lambda part: part[0].smallest_feature)
    self._sorted = True

  def larger_than(self, min_feature):
    if not self._sorted:
      self.sort()

    #TODO: Binary search for the first part larger than smallest_feature
    start = None
    for (i, (part, spots, weight)) in enumerate(self.parts):
      if part.smallest_feature >= min_feature:
        return (
        [(part, spots) for (part, spots, weight) in self.parts[i:]],
        [weight for (part, spots, weight) in self.parts[i:]]
        )

    return None, None

  def print(self):
    for (part, spots, weight) in self.parts:
      print(f"{part.name} min_feature: {part.smallest_feature}")

standard_size = 10
inventory = {}
for module in modules:    
  for (part, spots, weight) in module.render(standard_size):
    if part.shape not in inventory:
      inventory[part.shape] = PartInventory()  
    part.render_stl()
    inventory[part.shape].add(part, spots, weight)   

def random_part(spot, min_feature):
  #Building up the list this way seems slow. So many copies.
  parts = []
  weights = []
  for shape in inventory.keys():
    spot_size = spot.effective_size(shape)
    scale_factor = standard_size / spot_size 
    min_feature_if_standard_size = min_feature * scale_factor
    (shape_parts, shape_weights) = inventory[shape].larger_than(min_feature_if_standard_size)
    if shape_parts:
      parts.extend(shape_parts)
      weights.extend(shape_weights)
    #inventory[shape].print()

  if parts:
    choice = random.choices(parts, weights)[0]
    return copy.deepcopy(choice)
  else:
    return None, None