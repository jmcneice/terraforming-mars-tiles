import math
import solid
import numpy as np
from parts import tree

def hex(seed, max_height):
  num_sides = 6
  base_height = 2
  radius = 20.20 #Gives a diameter of 35

  # Rotated so a point is along the y-axis, a cyclinder of the base height with the right number of sides
  base = solid.rotate([0,0,30])( 
    solid.cylinder(r1=radius,r2=radius,h=base_height,segments=num_sides)
  )
  base -= _mirrored_seed(seed)

  #TODO: Calculate the 17.49 from the 20.20 radius
  tile = tree.Spot(tree.Shape.circle, 17.49, isCentralTower=True, maximum_height=max_height)
  tile.attach(
    tree.Part("hex_base", tree.Shape.circle, 17.49, 17.49, base),
    [tree.Spot(tree.Shape.circle, 17.49, (0,0,base_height), isCentralTower=True)],
  )
  return tile

def square(seed, base_height):
  size = 17.49 * math.sqrt(2)
  base = solid.utils.up(base_height/2)(solid.cube((size, size, base_height), center=True))
  base -= _mirrored_seed(seed)

  base_part = tree.Part("square_base", tree.Shape.square, size, base)
  spots = [tree.Spot(tree.Shape.square, size, (0,0,base_height), isCentralTower=True)]
  return base_part, spots

  return base, spots

def _mirrored_seed(seed):
  return solid.mirror([1,0,0]) (
    solid.linear_extrude(height = 1) (
      solid.text(text = str(seed), size = 11, halign = "center", valign = "center")      
    )
)