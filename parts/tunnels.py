import math
import solid
import itertools
import numpy as np
from parts import tree

def render(size):  
  parts = []

  possible_holes = range(5,9)  
  possible_height_ratios = np.linspace(0.5, 1.5, num = 3)
  possible_size_ratios = [0.9] #[0.9, 1]  

  for holes in possible_holes:
    for height_rato in possible_height_ratios:
      for size_ratio in possible_size_ratios:
        (solid, spots) = tunnels(size, holes, height_rato, size_ratio)
        parts.append((solid, spots))  
      
  possible_stacks = range(2,4)
  for height in possible_stacks: 
    (stack, stack_spots) = tunnel_stack(size, height, possible_holes)
    parts.append((stack, stack_spots))

  return [(solid, spots, 1/len(parts)) for (solid, spots) in parts]

def tunnel_stack(radius, num_stacked, possible_holes):
  spots = []
  top_translation = (0,0,0)
  stack_solid = solid.union()
  holes = itertools.cycle(possible_holes)
  smallest_feature = math.inf

  next_radius = radius
  for i in range (0, num_stacked):
    (level, level_spots) = tunnels(next_radius, next(holes), tower_top_ratio = 1)    

    for spot in level_spots[1:]:
      spot.location.translation = np.add(spot.location.translation, top_translation)
      spots.append(spot)    

    level_top_spot = level_spots[0]
    stack_solid += solid.translate(top_translation)(level.solid)

    next_radius = level_top_spot.size
    top_translation = np.add(top_translation, level_top_spot.location.translation)      
    smallest_feature = min(smallest_feature, level.smallest_feature)
  
  stack = tree.Part(f"tunnel_stack_{num_stacked}_{possible_holes}", tree.Shape.circle, radius, smallest_feature, stack_solid)
  spots.append(tree.Spot(tree.Shape.circle, next_radius, top_translation, isCentralTower=True))
  return stack, spots

def tunnels(radius, num_holes, tower_height_ratio = 0.6, tower_top_ratio = 0.9):
  tower_bottom = radius * 0.8
  tower_top = radius * tower_top_ratio  
  tower_height = radius * tower_height_ratio
  hole_percentage = 0.65
  bottom_circumference = 2 * math.pi * tower_bottom
  hole_radius = (bottom_circumference * hole_percentage / num_holes) / 2  
  hole_height = tower_height * 0.9 #0.66

  #Create a circular cylinder wider at the top, and wider than it is tall
  cylinder = solid.cylinder(r1 = tower_bottom, r2 = tower_top, h = tower_height, segments = 20)
  tunnels = tree.Part(
    f"tunnels_{num_holes}_{tower_height_ratio}_{tower_top_ratio}", 
    tree.Shape.circle, 
    radius, 
    tower_height - hole_height,
    cylinder,
  )

  spots = []
  spots.append(
    tree.Spot(
      tree.Shape.circle, 
      tower_top, 
      translation=(0,0,tower_height), 
      isCentralTower=True
    )
  )

  #An oval cylinder laying on its side half below on z-axis   
  spoke = solid.cylinder(r = hole_radius, h = radius, segments = 50)
  spoke = solid.scale([hole_height/hole_radius,1,1])(spoke)
  spoke = solid.rotate([0,90,0])(spoke)
  spoke = solid.translate((radius*0.50, 0, 0))(spoke)
  
  #Punch holes in the central cylinder by rotating the oval cylinder around
  #Set up cities in the tunnels 
  tunnel_city_size = hole_radius
  tunnel_city_maximum_height = hole_height * 0.9
  tunnel_city_distance = radius - tunnel_city_size
  for rotation in np.linspace(0, 360, num = num_holes, endpoint = False):
    tunnels.solid -= solid.rotate([0,0,rotation])(spoke)
    spots.append(
      tree.Spot(
        tree.Shape.circle, 
        tunnel_city_size, 
        maximum_height=tunnel_city_maximum_height, 
        translation=(tunnel_city_distance,0,0),
        rotation=(0,0,rotation),
      )
    )

  return tunnels, spots