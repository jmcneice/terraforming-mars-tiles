import math
import solid
from solid import utils
import numpy as np
from parts import tree

def render(size):
  (solid, spots) = detailed_tower(size)
  return [(solid, spots, 1)]

#"Tower" option: Make a tower with 4 legs and smaller cities on top and around it
def detailed_tower(width):  
  #Add a tower then a slightly wider thin tower overlapping for some detail  
  (thick_leg_tower, small_spots) = tower(4, width, 0.75, 0.1, 0.95)
  (thin_leg_tower, large_spots) = tower(4, width, 0.75, 0.05, 1)

  thick_leg_tower.solid += thin_leg_tower.solid
  thick_leg_tower.name = "detailed_tower"
  thick_leg_tower.smallest_feature = thin_leg_tower.smallest_feature
  return thick_leg_tower, small_spots

# diameter: The width of the area to fill. A cube is assumed.
# height_ratio: The percentage of the diameter to fill vertically
# leg_thickness_ratio: The percentage of the diameter that each leg should fill
# cut_distance_ratio: How far away the cut cylinders will be created. A larger percentage cuts less material.
# number of legs: How many legs to have. 2 to 8 make nice looking structures
def tower(number_legs, width, height_ratio, leg_thickness_ratio, cut_distance_ratio):
  base_height = width*height_ratio
  leg_thickness = width*leg_thickness_ratio

  tower = tree.Part("tower", tree.Shape.square, width, leg_thickness, solid.union())

  leg = solid.translate([width/4, 0, base_height/2])(solid.cube([width/2,leg_thickness,base_height],center=True))
  for rotation in np.linspace(0, 360, num = number_legs, endpoint = False):
    tower.solid += solid.rotate([0,0,rotation])(leg)

  #Upside down circular pyramid as the upper platform
  platform_start = base_height*0.6
  platform_height = base_height*1.1
  tower.solid += utils.up(platform_start)(solid.cylinder(r1 = 0, r2 = base_height*0.4, h = platform_height - platform_start))

  #Subtract out large cylinders placed at the edges. 
  cut_distance = base_height*cut_distance_ratio
  cut_height = base_height*0.65  
  cut_radius = base_height*0.8
  cut = solid.rotate((90, 0, 0))(solid.cylinder(r = cut_radius, h = width*1, center=True))
  cut = solid.translate((cut_distance, 0, cut_height))(cut)
  for position in np.linspace(0, 360, num = number_legs, endpoint = False):
    tower.solid -= solid.rotate([0,0,position])(cut)
  
  spots = []

  #Add an open area on top of the tower
  sagitta_of_platform_intersect = cut_radius - (platform_height - cut_height)
  half_chord_length_at_platform_intersect = math.sqrt(2*sagitta_of_platform_intersect*cut_radius - sagitta_of_platform_intersect**2)
  platform_width = (cut_distance - half_chord_length_at_platform_intersect) * 2  
  spots.append(
    tree.Spot(
      tree.Shape.square, 
      platform_width, 
      translation=(0,0,platform_height), 
      isCentralTower=True
    )
  )
    
  #Add open areas in the corners
  corner_city_width = (width/2 - leg_thickness)
  corner_city_distance = width/4             
  spots.extend(
    tree.Spot(
      tree.Shape.square,
      corner_city_width,
      translation=(corner_city_distance, corner_city_distance, 0)
    )
    .rotated(4)
  )   

  #Subtract out an oval from 0,0 to half way up    
  center_cut_radius = base_height*0.25
  tower.solid -= solid.scale([1,1,2])(solid.sphere(center_cut_radius))  
  spots.append(
    tree.Spot(
      tree.Shape.circle,
      center_cut_radius*0.9,
    )
  )   

  return tower, spots