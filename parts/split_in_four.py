import math
import solid
from solid import utils
import numpy as np

import parts
from parts import tree

def render(size):  
  #(solid, spots) = split_in_four(size)  
  #return [(solid, spots, 1)]
  #TODO: Figure out how to ensure split_in_four is filled if it's used
  return []

def split_in_four(width):
  seperation_distance = width * 0.1
  corner_city_width = (width - seperation_distance)/2
  corner_city_distance = width/4  
  corner_city_feature_size = width * 0.05 #Arbitrary. Trying to make sure the feature size represents if parts can fit in following the split.

  empty_part = tree.Part("split_in_four", tree.Shape.square, width, corner_city_feature_size, solid.union(), render=False)  

  spots = []
  spots.extend(
    tree.Spot(
      tree.Shape.square,
      corner_city_width,
      (corner_city_distance, corner_city_distance, 0),
    )
    .rotated(4)    
  )

  spots[0].isCentralTower = True

  return empty_part, spots