import math
import solid
from solid import utils
import numpy as np
from parts import tree

def player(base_width):
  #A player cube is 8mm and we add 1mm of give  
  player_cube = 8 + 1
  well_depth = 3
  wall_thickness = 1
  base_thicnkess = 1
  cut_out_height = well_depth * 2

  cube_holder_height = well_depth + base_thicnkess
  cube_holder_width = player_cube + wall_thickness
  cube_holder = solid.utils.up(cube_holder_height/2)(solid.cube((cube_holder_width, cube_holder_width, cube_holder_height), center=True))
  cube_well = solid.utils.up(base_thicnkess + cut_out_height/2)(solid.cube((player_cube, player_cube, cut_out_height), center=True))

  player_cube_holder = tree.Part(f"cube_holder_{base_width}", tree.Shape.square, base_width, wall_thickness, cube_holder - cube_well)

  if base_width < cube_holder_width:
    #The supporting column is constructed as a 4-sided cylinder for simplicity
    supporting_column_slope = math.radians(45)
    support_column_top_radius = cube_holder_width * math.sqrt(2) / 2
    support_column_bottom_radius = base_width * math.sqrt(2) / 2
    support_column_height = (support_column_top_radius - support_column_bottom_radius) / math.tan(supporting_column_slope)
    support_column = solid.rotate((0,0,45))(solid.cylinder(r1 = support_column_bottom_radius, r2 = support_column_top_radius, h = support_column_height, segments=4))
    player_cube_holder.solid = support_column + solid.utils.up(support_column_height)(player_cube_holder.solid)

  return player_cube_holder