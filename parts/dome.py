import math
import solid
import numpy as np
from parts import tree

from parts import fin_column

def render(size):  
  parts = []

  (banded_part, banded_spots) = banded_dome(size)
  parts.append((banded_part, banded_spots, 1))

  '''
  (dome_part, dome_spots) = dome(size)  
  parts.append((dome_part, dome_spots, 0))
  '''

  '''
  possible_arches = range(2,4)
  weight = 0 #1/len(possible_arches)
  for num_arches in possible_arches: 
    (arch_part, arch_spots) = arches(size, num_arches)
    parts.append((arch_part, arch_spots, weight))    
  '''
  
  '''
  possible_fins = range(4,7)
  weight = 1/len(possible_fins)  
  for fins in possible_fins: 
    (solid, spots) = raised_dome(size, fins)
    parts.append((solid, spots, weight))
  '''

  #(tall_dome, tall_spots) = raised_dome(size,10)  
  #parts.append((tall_dome, tall_spots, 1))    

  return parts

def banded_dome(radius):
  arch_thickness_ratio = 0.2  
  dome_radius = radius*(1 - arch_thickness_ratio)
  
  (outer_arches, arch_spots) = arches(radius, 2, 0.8, arch_thickness_ratio)
  (inner_dome, dome_spots) = dome(dome_radius)
  
  banded_dome = tree.Part("banded_dome", tree.Shape.circle, radius, outer_arches.smallest_feature, outer_arches.solid + inner_dome.solid)
  banded_dome_spots = arch_spots[:1] #Only continue building on top of the arches
  
  return banded_dome, banded_dome_spots

def raised_dome(radius, fins):
  (column, column_spots) = fin_column.fin_column(radius, fins)  
  (banded_dome_part, banded_dome_spots) = banded_dome(column_spots[0].size)
  
  raised_dome_solid = column.solid + solid.translate(column_spots[0].location.translation)(banded_dome_part.solid)
  raised_dome_part = tree.Part(f"raised_dome_{fins}", tree.Shape.circle, radius, banded_dome_part.smallest_feature, raised_dome_solid)

  raised_dome_spots = [tree.Spot(
    tree.Shape.circle,
    banded_dome_spots[0].size,
    translation= np.add(banded_dome_spots[0].location.translation, column_spots[0].location.translation),
    isCentralTower=True
  )]

  return raised_dome_part, raised_dome_spots

def arches(radius, num_arches = 2, opacity = 0.8, thickness_ratio = 0.2):
  arch_height = radius
  arch_thickness = radius * thickness_ratio
  circumference = 2 * math.pi * radius
  arch_width = (circumference * (1-opacity) / num_arches) / 2  
  
  arches = tree.Part(f"arches_{num_arches}", tree.Shape.circle, radius, arch_thickness, solid.union())

  x_arch = solid.intersection()(
    solid.sphere(arch_height),
    solid.utils.up(arch_height/2)(solid.cube([arch_width,radius*2,arch_height], center = True))
  )
  
  #Spin the x_arch around to generate all of the arcs  
  for rotation in np.linspace(0, 180, num = num_arches, endpoint = False):
    arches.solid += solid.rotate([0,0,rotation])(x_arch)  

  #Cut out the center
  arches.solid -= solid.sphere(radius - arch_thickness)

  spots = []

  #Add upper build area
  upper_build_area_size = arch_width
  upper_build_area_height = arch_height - (arch_thickness*0.1) #TODO: Calculate the intersection height of the arches rather than having this 0.1 fudge factor
  spots.append(
    tree.Spot(
      tree.Shape.square,
      upper_build_area_size,
      (0,0,upper_build_area_height),
      isCentralTower=True,
    )
  ) 

  #Add inner build area
  buildable_inner_area = (radius - arch_thickness)*0.8
  spots.append(
    tree.Spot(
      tree.Shape.circle,
      buildable_inner_area, 
      maximum_height=upper_build_area_height  #TODO: The arches need support so if anything we'd like a minimum height inside instead of a minimum
    )
  )

  return arches, spots

def dome(radius):
  dome = tree.Part(f"dome", tree.Shape.circle, radius, radius*2, solid.sphere(radius, segments=25))
  dome.solid -= (solid.translate((0,0,-1*radius/2))
    (solid.cube([radius*2,radius*2,radius],center=True))
  ) 
  return dome, []