import math
import random
import copy 
import time
import numpy as np
import solid
import solid.utils
import parts.base
from parts import tree
from parts import cube_holder
SEGMENTS = 100

def generate_tree(radius, trunk_height, reduction_ratio):
  trunk_radius = 2
  trunk = solid.cylinder(r1 = trunk_radius, r2 = trunk_radius, h=trunk_height) #TODO: Varying from *0.1 to *0.05 gives a nice curve to it

  spread_angle = 60
  spread_height = (radius - trunk_radius) / math.tan(math.radians(spread_angle))
  spread = solid.cylinder(r1=trunk_radius,r2=radius,h=spread_height)
  trunk += solid.translate((0,0,trunk_height))(spread)

  first_branch_height = trunk_height+spread_height
  branch_width = 1 #TODO: Calculate it
  branch_radius = radius / 1.2
  branch_count = 0  

  while branch_radius > 2: #for i in range(1,num_branches+1):
    branch = solid.cylinder(r1 = branch_radius * 1.2, r2 = branch_radius, h=branch_width, center=True)
    branch = solid.translate((0, 0, branch_width/2 + first_branch_height + branch_width*branch_count))(branch)
    trunk += branch

    branch_count += 1
    branch_radius = branch_radius*reduction_ratio

  cap = solid.cylinder(r1 = branch_radius * 1.2, r2 = 0, h=branch_width*2, center=True)
  cap = solid.translate((0, 0, branch_width + first_branch_height + branch_width*branch_count))(cap)
  trunk += cap

  return tree.Part(f"tree", tree.Shape.circle, radius, trunk_radius, trunk, render='False')      

def generate_forest(num_trees = 10, max_tries = 100000):
  hex = parts.base.hex("James", 100)  
  hex.spots = []
  base_height = 2
  hex_radius = 20.20

  cube_holder_spot = tree.Spot(tree.Shape.square, 10, translation=(hex_radius/2,0,base_height)) 
  cube_holder_spot.attach(cube_holder.player(10))
  hex.spots.append(cube_holder_spot)

  trunk_height = [2,3]
  tree_radius = [4,6]  
  tree_reduction_ratio = [0.88,0.92]
  tries = 0
  while(len(hex.spots) < num_trees and tries < max_tries):
    tries += 1
    radius = random.uniform(*tree_radius)    
    [(x, y)] = random_points_in_hexagon(hex_radius - (radius*1.2), 1)    
    spot = tree.Spot(tree.Shape.circle, radius, translation=(x,y,base_height)) 

    if(any(existing_spot.overlaps(spot) for existing_spot in hex.spots)):      
      continue
    else:
      spot.attach(generate_tree(radius, random.uniform(*trunk_height), random.uniform(*tree_reduction_ratio)))
      hex.spots.append(spot)      

  print(f"Have {len(hex.spots)} spots")
  return hex.render_scad()

def random_points_in_hexagon(radius, num_points):
  random_points = []
  while(len(random_points) < num_points):
    x = random.uniform(-1 * radius, radius)
    y = random.uniform(-1 * radius, radius)
    if(is_point_in_hexagon(radius, x, y)):
      random_points.append((x,y))
  
  return random_points

#Assumes the hexagon is rotated so the point is aligned along y
def is_point_in_hexagon(radius, x, y):
  #Normalize the point to be in quadrant 1
  x = abs(x)
  y = abs(y)  
  
  #If the point is beyond the right edge of the hexagon, it's not inside
  h = radius*math.cos(math.radians(30))  
  if(x > h):
    return False
  
  #If the point is above the line, it's not inside
  hex_edge_y = radius - x/math.sqrt(3)
  if(y > hex_edge_y):
    return False

  return True

if __name__ == '__main__':        
  #forest = generate_canopy()
  #forest = test_spheres()
  #forest = generate_central_trunk()
  forest = generate_forest()
  #forest = stack_different_cylinders()
  solid.scad_render_to_file(forest, file_header=f'$fn = {SEGMENTS};', include_orig_code=False)  

  #points = random_points_in_hexagon(10, 5)
  #print(points)